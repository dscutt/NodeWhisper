/*
	SERVER PROCESS
		by Daniel Scutt
	1) Test of connections.js as a server.
	2) Compatible thru V0.03
*/


/* Required Functions */


/* Imports */
// Connections class
var conn_class = require('./connections.js');


/* Variables */
// Connection Validation Data
var time_to_wait_for_response = 5000;
var maximum_connection_attempts = 10;
// Local data
var local_ip = '127.0.0.1';
var local_port = 5000;
// Server Connection Names
var conn1 = "Server1";
var conn2 = "Portal1";

/* Objects */
var connection = new conn_class.Connections("Server");


/* Logic */
connection.SetCommandListener();
connection.allowpermanentconnections();
connection.LoopLogic();
connection.SetupConnectionsHandler();
connection.buildconnection(conn1,-1,false,"udp4",local_ip,local_port);
connection.buildconnection(conn2,-1,false,"test_child.js");

connection.SendMessage(conn2,"TESTCONN")

connection.ConnectionsHandler.on('receive',(args) => {
	try {
		var c = args[0];
		var t = args[1];
		var m = args[2];
		var r = false;
		if (t == "socket" && args.length == 4) r = args[3];
		
		// Parse Message
		var reply = false;
		var msg = connection.parsemessage(m);
		var title = false;
		if ('title' in msg) title = msg['title'];
		
		// Look For Arguments
		if (title == "TESTCONN") {
			reply = connection.formatmessage({'title':'CONNSAT','reply':'test sat'});
		} else if (title == "CONNSAT" && 'reply' in msg) {
			connection.print("From "+c+" - "+msg['reply']);
		}
		
		// Send reply if required
		if (reply && t == "socket") {
			// Reply via socket
			connection.SendMessage(c,reply,r.address,r.port);
		} else if(reply && t == "child") {
			// Reply via relationship
			connection.SendMessage(c,reply);
		} else if (reply && t == "parent") {
			// Reply to parent
			connection.SendMessage("parent",reply);
		}
	} catch(err) {
		connection.print("ConnectionsHandler failed on error: "+err)
	}
});











